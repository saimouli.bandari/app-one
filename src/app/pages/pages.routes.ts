import { Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LivePageComponent } from './live-page/live-page.component';

export const routes: Routes = [
    {
        path: "",
        component: PagesComponent,
        children: [
            {
                path: "dashboard",
                component: DashboardComponent
            },
            {
                path: "live-page",
                component: LivePageComponent
            },
            {
                path: "",
                redirectTo: "dashboard",
                pathMatch: "full"
            }
        ]
    }
];
